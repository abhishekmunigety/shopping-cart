﻿using FinalShopCart.Models;
using FinalShopCart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace FinalShopCart.Controllers
{
    public class CustomerController : Controller
    {
        private EcartDb2Entities1 objECartDbEntities;
        private List<ShoppingCartModel> listOfShoppingCartModels;

        public CustomerController()
        {
            objECartDbEntities = new EcartDb2Entities1();
            listOfShoppingCartModels = new List<ShoppingCartModel>();
        }
        public ActionResult Index()
        {
            
            return View();
          
        }
        [HttpPost]
       
        public ActionResult Index(Customer _objModelMail)

        {
            var customer = objECartDbEntities.Customers.FirstOrDefault(c => c.Email == _objModelMail.Email);
            int customerId;

            if(customer == null)
            {
                customerId =  objECartDbEntities.Customers.Max(c => c.CustomerId) + 1;
            }
            else
            {
                customerId = customer.CustomerId;
            }
            

            int OrderId = 0;
            listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            Order orderObj = new Order()
            {
                OrderDate = DateTime.Now,
                OrderNumber = String.Format("{0:ddmmyyyyHHmmsss}", DateTime.Now)
            };
            objECartDbEntities.Orders.Add(orderObj);
            objECartDbEntities.SaveChanges();
            OrderId = orderObj.OrderId;

            foreach (var item in listOfShoppingCartModels)
            {

                OrderDetail objOrderDetail = new OrderDetail();
                objOrderDetail.Total = item.Total;
                objOrderDetail.ItemId = item.ItemId;
                objOrderDetail.OrderId = OrderId;
                objOrderDetail.Quantity = item.Quantity;
                objOrderDetail.UnitPrice = item.UnitPrice;
                objOrderDetail.SubTotal = item.SubTotal;
                objOrderDetail.Tax = item.Tax;
                objOrderDetail.GrandTotal = item.GrandTotal;
                objOrderDetail.CustomerId = customerId;



                objECartDbEntities.OrderDetails.Add(objOrderDetail);
                if(customer != null)
                {
                    objECartDbEntities.SaveChanges();
                }
            }

            Session["CartItem"] = null;
            Session["CartCounter"] = null;

            if (ModelState.IsValid)
            {
                Item id = new Item();
                int recentOrderId = objECartDbEntities.OrderDetails.Max(t => t.OrderId);
                //var z= db.OrderDetails.Find(20);
                var blogs = from b in objECartDbEntities.OrderDetails where b.OrderId==recentOrderId select b;
                var orders = objECartDbEntities.OrderDetails.Where(o => o.OrderId == recentOrderId);
                string emailToAddress = _objModelMail.Email;// "abhishekmunigety@gmail.com"; //Receiver Email Address 
                string subject = "Congratulations";

                decimal subTotal = 0;
                decimal tax = 0;
                decimal grandTotal = 0;

                foreach (var order in orders)
                {
                    tax += order.Tax;
                    subTotal += order.Total;
                }

                grandTotal = tax + subTotal;

                //string body = "Congratulations " + _objModelMail.FirstName + ",</br>You have ben successfull Ordered. "+id.ItemPrice+"is price and product is"+id.ImagePath;
                string body = @"<html>
                      <body>
                      <p> Dear " + _objModelMail.FirstName + " </p> " +
                      "<p> You have been successfull Ordered</p>";
                body = body + @"

<table>
<tr>
<th>item</th>
<th>quantity</th>
<th>Price</th>
<th>total</th>

</tr>";
                foreach (var a in blogs) {
                    var i = from b in objECartDbEntities.Items where b.ItemId.ToString()==a.ItemId select b;
                    body = body + @"<tr>
<td>" + i.FirstOrDefault().ItemName + @"</td>
<td></td>





<td>" + a.Quantity + @"</td>
<td>" + a.UnitPrice + @"</td>
<td>" + a.Total + @"</td>


</tr>";
                }
                body = body + @"<tr>
<th>subTotal</th>
<th></th>
<th></th>
<th></th>
<th>" + subTotal;
              

 body = body + @"</th></tr><tr>
<th>Tax  </th>
<th></th>
<th></th>
<th></th>
<th> " + tax;
               

body = body + @"</th></tr><tr>
<th>GrandTotal</th >
<th></th>
<th></th>
<th></th>
<th> " + grandTotal;
                body = body + @"</tr>



</table>


                      
                      </body>
                      </html>
                     ";
               
                SendEmail(emailToAddress, subject, body);

                objECartDbEntities.Customers.Add(_objModelMail);
                objECartDbEntities.SaveChanges();
            
             return RedirectToAction("index","shopping");
             }

            else
            {
                return RedirectToAction("index", "shopping");
            }
        }

        public void SendEmail(string emailToAddress, string subject, string body)

        {
            string smtpAddress = "smtp.gmail.com";
            int portNumber = 587;
            bool enableSSL = true;
            string emailFromAddress = "abhishekmunigety@gmail.com"; //Sender Email Address 
            string password = "wxwefgfmokzlfprp"; //Sender Password 

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFromAddress);
                mail.To.Add(emailToAddress);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
               /* mail.Attachments.Add(new Attachment("fileUploader"));//*//*--Uncomment this to send any attachment */
                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFromAddress, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);

                }

            }

        }
    }
}
