# README #

Shopping Cart Application
Shopping Cart Mvc Aplication is designed with following Features(adding Cart(buying products),Updating cart,deleting Cart,Generate Billing receipt and send it to GMail).
I have used Visual studio 2019 edition ,SQL Server management studio softwares to develop The Shopping Cart application


#Concepts Used to build the Shopping Cart Application.#

The Application build using Concepts of MVC database First Approach
Using Unobstrive Jquery Validations
Using Javascripts , sessions , Email , Partial View
Using LinQ and get the Billing receipt from database


#Steps Used to Create the application#

Create a new database in SQL server management studio and create schemas master, Order and customer
create Tables for these 3 schemas Master.items for product - To create product and add it to cart
Order.Orderdetails - to add the product to the billing cart
Customer.Customers - to create a Customer billing information Page and send him Mail
Create a Shopping-Cart Project in visual studio (asp.net web application(.Net framework) select Mvc and unit test cases )
Download the required packages from manage Nuget Pasckages - Unobstrive Jquery validations , Bootstrap ,.Net entity framework Tools
create a Model -Ado.Net entity Framework Model and get the database from server management studio using database First Approach
Dont forget to rebuild the Application
Create 3 controller for customers , Items and order
Join Customers and Items , Items and order using LinQ concepts and genetrate View for them
Email operations is used in Customer controller to send Billing receipt to the customer
Generate a partial View for cart 



#How To Run Application#

Start The Application
there are three buttons on the webpage - Items , Shopping and Cart
Cart - Intially the cart is empty (no products in Cart )
Shopping - to add the Products into the cart
Items - to create a new product and add it to shopping cart 
After selecting The items which you need to buy
click on quantity to select Quantity - min is 1 and maximum is 10
and click on update cart to update the Cart
click on delete symbol in action to delete the item from the cart
If you want to select few more items into the cart click on back to shopping Button
click on Next to go into the customer details page
give customers information and click on create to buy the product and the billing information is sent to your mail
Click on reset to reset The Customer page