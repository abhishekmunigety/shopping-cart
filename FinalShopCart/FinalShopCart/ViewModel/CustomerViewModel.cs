﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalShopCart.ViewModel
{
    public class CustomerViewModel
    {
       
    
            public int CustomerId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string BillingAddressLine1 { get; set; }
            public string BillingAddressLine2 { get; set; }
            public string BillingCity { get; set; }
            public string BillingState { get; set; }
            public string BillingCountry { get; set; }
            public string BillingZipCode { get; set; }
            public string ShippingAddressLine1 { get; set; }
            public string ShippingAddressLine2 { get; set; }
            public string ShippingCity { get; set; }
            public string ShippingState { get; set; }
            public string ShippingCountry { get; set; }
            public string ShippingZipCode { get; set; }
        }
    }
