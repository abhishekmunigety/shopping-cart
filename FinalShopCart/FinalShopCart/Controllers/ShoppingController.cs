﻿using FinalShopCart.Models;
using FinalShopCart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalShopCart.Controllers
{
 
    public class ShoppingController : Controller
    {
        private EcartDb2Entities1 objECartDbEntities;
        private List<ShoppingCartModel> listOfShoppingCartModels;
        public ShoppingController()
        {
            objECartDbEntities = new EcartDb2Entities1();
            listOfShoppingCartModels = new List<ShoppingCartModel>();
        }
        // GET: Shopping
        public ActionResult Index()
        {
            IEnumerable<ShoppingViewModel> listOfShoppingViewModels = (from objItem in objECartDbEntities.Items
                                                                       join
                                                                           objCate in objECartDbEntities.Categories
                                                                           on objItem.CategoryId equals objCate.CategoryId
                                                                       select new ShoppingViewModel()
                                                                       {
                                                                           ImagePath = objItem.ImagePath,
                                                                           ItemName = objItem.ItemName,
                                                                           Description = objItem.Description,
                                                                           ItemPrice = objItem.ItemPrice,
                                                                           ItemId = objItem.ItemId,
                                                                           Category = objCate.CategoryName,
                                                                           ItemCode = objItem.ItemCode
                                                                       }

                ).ToList();
            return View(listOfShoppingViewModels);
        }

        [HttpPost]
        public JsonResult Index(string ItemId)
        {
            ShoppingCartModel objShoppingCartModel = new ShoppingCartModel();
            Item objItem = objECartDbEntities.Items.Single(model => model.ItemId.ToString() == ItemId);
            if (Session["CartCounter"] != null)
            {
                listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            }
           
            if (listOfShoppingCartModels.Any(model => model.ItemId == ItemId))
            {

                objShoppingCartModel = listOfShoppingCartModels.Single(model => model.ItemId == ItemId);

                objShoppingCartModel.Quantity = objShoppingCartModel.Quantity + 1;

                objShoppingCartModel.Total = objShoppingCartModel.Quantity * objShoppingCartModel.UnitPrice;
                objShoppingCartModel.SubTotal =  objShoppingCartModel.Total;
                objShoppingCartModel.Tax = objShoppingCartModel.SubTotal * 10/100;
                objShoppingCartModel.GrandTotal = objShoppingCartModel.SubTotal + objShoppingCartModel.Tax;

            }
            else
            {
                objShoppingCartModel.ItemId = ItemId;
                objShoppingCartModel.ImagePath = objItem.ImagePath;
                objShoppingCartModel.ItemName = objItem.ItemName;
                objShoppingCartModel.Quantity = 1;
                objShoppingCartModel.Total = objItem.ItemPrice;
                objShoppingCartModel.UnitPrice = objItem.ItemPrice;
                objShoppingCartModel.SubTotal = objItem.ItemPrice;
                objShoppingCartModel.GrandTotal = objItem.ItemPrice + objItem.ItemPrice * 10 / 100;
                objShoppingCartModel.Tax = objItem.ItemPrice * 10 / 100;


                listOfShoppingCartModels.Add(objShoppingCartModel);
            }

            Session["CartCounter"] = listOfShoppingCartModels.Count;
            Session["CartItem"] = listOfShoppingCartModels;
            return Json(new { Success = true, Counter = listOfShoppingCartModels.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ItemsCounter(string ItemId,int quantity)
        {
            ShoppingCartModel objShoppingCartModel = new ShoppingCartModel();
            Item objItem = objECartDbEntities.Items.Single(model => model.ItemId.ToString() == ItemId);
            if (Session["CartCounter"] != null)
            {
                listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            }
            if (listOfShoppingCartModels.Any(model => model.ItemId == ItemId))
            {
                objShoppingCartModel = listOfShoppingCartModels.Single(model => model.ItemId == ItemId);

                objShoppingCartModel.Quantity = quantity;

                objShoppingCartModel.Total = objShoppingCartModel.Quantity * objShoppingCartModel.UnitPrice;
                objShoppingCartModel.SubTotal = objShoppingCartModel.Total;
                objShoppingCartModel.Tax = objShoppingCartModel.SubTotal * 10 / 100;
                objShoppingCartModel.GrandTotal = objShoppingCartModel.SubTotal + objShoppingCartModel.Tax;
            }

            Session["CartCounter"] = listOfShoppingCartModels.Count;
            Session["CartItem"] = listOfShoppingCartModels;
            return Json(new { Success = true, Counter = listOfShoppingCartModels.Count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShoppingCart()
        {
            listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            return View(listOfShoppingCartModels);
        }

        [HttpPost]
        public ActionResult AddOrder()
        {
            int OrderId = 0;
            listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            Order orderObj = new Order()
            {
                OrderDate = DateTime.Now,
                OrderNumber = String.Format("{0:ddmmyyyyHHmmsss}", DateTime.Now)
            };
            objECartDbEntities.Orders.Add(orderObj);
            objECartDbEntities.SaveChanges();
            OrderId = orderObj.OrderId;

            foreach (var item in listOfShoppingCartModels)
            {
        
                OrderDetail objOrderDetail = new OrderDetail();
                objOrderDetail.Total = item.Total;
                objOrderDetail.ItemId = item.ItemId;
                objOrderDetail.OrderId = OrderId;
                objOrderDetail.Quantity = item.Quantity;
                objOrderDetail.UnitPrice = item.UnitPrice;
                objOrderDetail.SubTotal = item.SubTotal;
                objOrderDetail.Tax = item.Tax;
                objOrderDetail.GrandTotal = item.GrandTotal;
               
           
               

                objECartDbEntities.OrderDetails.Add(objOrderDetail);
                objECartDbEntities.SaveChanges();
            }

            Session["CartItem"] = null;
            Session["CartCounter"] = null;
            //return RedirectToAction("Index");
            return RedirectToAction("Index", "Customer");
        }

        public ActionResult Remove(string id)
        {

            listOfShoppingCartModels = Session["CartItem"] as List<ShoppingCartModel>;
            ShoppingCartModel c = listOfShoppingCartModels.Where(x => x.ItemId == id).SingleOrDefault();
            listOfShoppingCartModels.Remove(c);

            decimal h = 0;
            foreach (var item in listOfShoppingCartModels)
            {
                h += item.Total;

            }
            TempData["total"] = h;
            TempData.Keep();
            return RedirectToAction("ShoppingCart");

        }

        //public ActionResult next()
        //{
            
        //    if(listOfShoppingCartModels.Count > 0)
        //    {
        //        return View("Index","Customer");
        //    }
        //    else


        //    return View("Index");
        //}

        //public ActionResult next()
        //{
           
        //        if (true)
        //        return RedirectToAction("Index");

        //    else

        //    return RedirectToAction("Index", "Home");
   
        
        
        //}
        public ActionResult next()
        {
            if(Session["cartCount"]==null)
            {
                return RedirectToAction("_shoppingCart");

            }
            else
            {
                return RedirectToAction("Index", "customer");
            }
        }

        //public ActionResult UpdateCart(FormCollection frc)
        //{
        //    string[] quantities = frc.GetValues("quantity");
        //    List<ShoppingCartModel> lsCart = (List<ShoppingCartModel>)Session["CartItem"];
        //    for (int i = 0; i < lsCart.Count; i++)
        //    {
        //        lsCart[i].Quantity = Convert.ToInt32(quantities[i]);
        //    }
        //    Session["CartItem"] = lsCart;
        //    return View("ShoppingCart");
        //}
        public ActionResult UpdateCart()
        {
            //string[] quantities = frc.GetValues("quantity");
            //    List<ShoppingCartModel> lsCart = (List<ShoppingCartModel>)Session["CartItem"];
            //   for (int i = 0; i < lsCart.Count; i++)
            //   {
            //        lsCart[i].Quantity = Convert.ToInt32(quantities[i]);
            //    }
            //   Session["CartItem"] = lsCart;
            //  //return View("ShoppingCart");
            return RedirectToAction("ShoppingCart");
        }
    }








}