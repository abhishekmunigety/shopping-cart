﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalShopCart.ViewModel
{
    public class ShoppingCartModel
    {
        public string ItemId { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }

        public decimal SubTotal { get; set; }
        public decimal GrandTotal { get; set; }

        public decimal Tax { get; set; }
       

        public string ImagePath { get; set; }
        public string ItemName { get; set; }
    }
}